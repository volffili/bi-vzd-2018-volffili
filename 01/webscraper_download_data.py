from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
import re
from collections import namedtuple
import csv
import sys

LinkToThesis = namedtuple("LinkToThesis", "bcOrMgr nameOfThesis year link")
ThesisAdvancedData = namedtuple("ThesisAdvancedData", "bcOrMgr nameOfThesis year link")
NameWithTitles = namedtuple("NameWithTitles", "titles name")

if(len(sys.argv) < 2):
	print ('argument1: "onlyBasic"/"onlyAdvanced"/"both"')
	print ("onlyBasic saves the parsed data to basiData.csv, it's columns are: type,name,year,link")
	print ('onlyAdvanced opens basicData.csv and parses more information and saves it to advancedData.csv')
	print ('both first performs onlyBasic then onlyAdvanced mode')
	print ("argument2: number of theses to parse, has to be multiple of 50+1, e.g. 2501 (default is 2501)")
	print ("argument3: year of theses to search for (if omitted, it will search for any year)")
	exit()
####################################################################
#   I followed a tutorial on webscrapping I found on the internet  # 
#https://realpython.com/python-web-scraping-practical-introduction/#
####################################################################

def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200 
            and content_type is not None 
            and content_type.find('html') > -1)


def log_error(e):
    """
    It is always a good idea to log errors. 
    This function just prints them, but you can
    make it do anything.
    """
    print(e)

####################################################################
#         I copied the 3 methods above from the tutorial           #
####################################################################

def parseBasicInfo(beautifulSoupHtml):

	#for storing the final information that is parsed
	collectedLinks = []

	#this ugly thing just goes down the dom hierarchy to the things I am interested in
	for dd in beautifulSoupHtml.select('div'):
		if 'id' in dd.attrs:
			if dd['id'] == 'aplikace':
				for d in dd.select('div'):
					if 'style' in d.attrs:
						#these are the divs I am interested in
						if d['style'] == 'margin-left:50px':
							isBc = re.search("Obhajoba bakalářské práce: ",d.text)
							isMgr = re.search("Obhajoba diplomové práce: ",d.text)

							#just check whether it's really a div i am interested in
							#if there is no info about bachelor/diploma thesis i just skip it
							if((isBc != None)  != (isMgr != None)):
								for a in d.select('a'):
									#linkToMoreInfo is a link to the actual thesis
									linkToMoreInfo = a['href']
									if linkToMoreInfo[-1:] == '/':
										if linkToMoreInfo[:1] == '/':

											thesisType = "bc"
											if(isMgr):
												thesisType = "mgr"

											#parse the information I need and save it to a structure
											rokObhajoby = re.search("Rok: [0-9][0-9][0-9][0-9]",d.text).group()[-5:]
											nazevPrace = a.find('i').text
											linkWithInfo = LinkToThesis(thesisType,nazevPrace,rokObhajoby,linkToMoreInfo)
											collectedLinks.append(linkWithInfo);

	return collectedLinks

def debugPrintName(human):
	for title in human.titles:
		print(title)
	print(human.name)

							
def parseNameWithTitles(stringToParse):

	titles = [];
	while True:
		titul = re.search("^[a-zA-Z\.]+\.",stringToParse)
		if titul == None:
			break;

		titul = titul.group().strip().lower()
		titles.append(titul)
		stringToParse = re.sub("^[a-zA-Z\.]+\.",'',stringToParse).strip()

	stringDivided = stringToParse.split(',')
	name = stringDivided.pop(0).strip();

	for part in stringDivided:
		part = part.strip()							
		titul = re.search("^[a-zA-Z\.]+\.",part)
		if titul == None:
			continue;
		titul = titul.group().strip().lower()
		titles.append(titul)

	return NameWithTitles(titles,name)

#oneLinkWithInfo structure that contains basic information and link to the thesis page
def parseInfoFromThesisPage(oneLinkWithInfo):
	#create link to the thesis page

	finallink = "https://is.ambis.cz"+oneLinkWithInfo.link
	raw_inner_html = simple_get(finallink)
	inner_html = BeautifulSoup(raw_inner_html, 'html.parser')

	dataToReturn = []
	dataToReturn.append(oneLinkWithInfo.bcOrMgr)
	dataToReturn.append(oneLinkWithInfo.nameOfThesis)
	dataToReturn.append(oneLinkWithInfo.year)

	print(oneLinkWithInfo.nameOfThesis)
	#go down the dom hierarchy

	for div in inner_html.select('div'):
		if 'class' in div.attrs:
			#div that we are interested in
			if div['class'][0] == 'oddil':
				texts = []
				for h3 in div.select('h3'):
					texts.append(h3.text)
				for h4 in div.select('h4'):
					texts.append(h4.text)
				
				#not interested in the thesis, if I can't find field Vedouci and Oponent
				if(len(texts) >= 3):
					if texts[1] == 'Vedoucí:':
						if texts[2] == 'Oponent:':
							vedouciToParse = div.contents[6].find_next('li').text
							oponentToParse = div.contents[9].find_next('li').text

							vedouci = parseNameWithTitles(vedouciToParse);
							oponent = parseNameWithTitles(oponentToParse);

							dataToReturn.append(vedouci.name);
							dataToReturn.append(vedouci.titles);
							dataToReturn.append(oponent.name);
							dataToReturn.append(oponent.titles);
	return dataToReturn;

if(sys.argv[1] == "onlyBasic" or sys.argv[1] == "both"):
	with open('basicData.csv', 'w', newline='', encoding='utf-8') as csvfile:
		advancedDataWriter = csv.writer(csvfile, delimiter='%',
	                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
		advancedDataWriter.writerow(['type'] + ["name"] + ["year"] + ["link"])	

		until = 2501
		if len(sys.argv) > 2:
			until = int(sys.argv[2])

		rok = '-'
		if len(sys.argv) > 3:
			rok = sys.argv[3]

		
		for i in range(1,until,50):
			raw_html = simple_get("https://is.ambis.cz/thesis/?FAK=6110;PRI=-;ROK="+rok+";TIT=-;PRA=-;vypsat=1;exppar=1;por="+str(i))
			print(str(i)+". html length: ")
			print(len(raw_html))
			html = BeautifulSoup(raw_html, 'html.parser')
			collectedInfo = parseBasicInfo(html)
			for rowBasicInfo in collectedInfo:
				row = []
				row.append(str(rowBasicInfo.bcOrMgr))
				row.append(str(rowBasicInfo.nameOfThesis))
				row.append(str(rowBasicInfo.year))
				row.append(str(rowBasicInfo.link))
				#row = ['a'] + ['b'] + ['c'] + ['d']
				print(row)
				advancedDataWriter.writerow( row )	

numOfRows = 0
if sys.argv[1] == "onlyAdvanced":
	lastNameOfThesis = ""

	try:
		with open('advancedData.csv', 'r+', newline='', encoding='utf-8') as csvfile:
			advancedDataReader = csv.reader(csvfile, delimiter='%', quotechar='|', quoting=csv.QUOTE_MINIMAL)

			for row in advancedDataReader:	
				lastNameOfThesis = row[1]								
				numOfRows += 1

			print(numOfRows)
				
	except IOError:
		with open('advancedData.csv', 'w', newline='', encoding='utf-8') as csvfile:
			print("advancedData.csv created")

	with open('basicData.csv', 'r' ,newline='', encoding='utf-8') as csvfile:
		shouldAppendToCollect = False
		basicDataReader = csv.reader(csvfile, delimiter='%', quotechar='|')
		collectedInfo = []
		
		skip = 1
		for row in basicDataReader:											
			if(skip):
				skip = 0
				continue

			linkWithInfo = LinkToThesis(row[0],row[1],row[2],row[3])
		
			if( shouldAppendToCollect == False and (lastNameOfThesis == row[1] or numOfRows <= 1)):
				print("loading basicData from "+lastNameOfThesis)
				shouldAppendToCollect = True
				continue
			if(shouldAppendToCollect):
				collectedInfo.append(linkWithInfo)

if(sys.argv[1] == "onlyAdvanced" or sys.argv[1] == "both"):
	with open('advancedData.csv', 'a', newline='', encoding='utf-8') as csvfile:
		advancedDataWriter = csv.writer(csvfile, delimiter='%', quotechar='|', quoting=csv.QUOTE_MINIMAL)
		if numOfRows == 0:
			advancedDataWriter.writerow(['type'] + ["name"] + ["year"] + ["vedouci"] + ["vedouciTituly"] + ["oponent"] + ["oponentTituly"])	
		
		for oneLinkWithInfo in collectedInfo:
			finalDataToWrite = parseInfoFromThesisPage(oneLinkWithInfo)
			advancedDataWriter.writerow(finalDataToWrite)
			
