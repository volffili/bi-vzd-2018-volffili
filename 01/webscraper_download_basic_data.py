from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
import re
from collections import namedtuple
import csv
import sys

LinkToThesis = namedtuple("LinkToThesis", "bcOrMgr nameOfThesis year link")
ThesisAdvancedData = namedtuple("ThesisAdvancedData", "bcOrMgr nameOfThesis year link")

if(len(sys.argv) < 2):
	print ('argument1: "onlyBasic"/"onlyAdvanced"/"both"')
	print ("onlyBasic saves the parsed data to basiData.csv, it's columns are: type,name,year,link")
	print ('onlyAdvanced opens basicData.csv and parses more information and saves it to advancedData.csv')
	print ('both first performs onlyBasic then onlyAdvanced mode')
	print ("argument2: number of theses to parse, has to be multiple of 50+1, e.g. 2501 (default is 2501)")
	print ("argument3: year of theses to search for (if omitted, it will search for any year)")
	exit()
####################################################################
#   I followed a tutorial on webscrapping I found on the internet  # 
#https://realpython.com/python-web-scraping-practical-introduction/#
####################################################################

def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200 
            and content_type is not None 
            and content_type.find('html') > -1)


def log_error(e):
    """
    It is always a good idea to log errors. 
    This function just prints them, but you can
    make it do anything.
    """
    print(e)

####################################################################
#         I copied the 3 methods above from the tutorial           #
####################################################################

def parseBasicInfo(beautifulSoupHtml):

	#for storing the final information that is parsed
	collectedLinks = []

	#this ugly thing just goes down the dom hierarchy to the things I am interested in
	for dd in beautifulSoupHtml.select('div'):
		if 'id' in dd.attrs:
			if dd['id'] == 'aplikace':
				for d in dd.select('div'):
					if 'style' in d.attrs:
						#these are the divs I am interested in
						if d['style'] == 'margin-left:50px':
							isBc = re.search("Obhajoba bakalářské práce: ",d.text)
							isMgr = re.search("Obhajoba diplomové práce: ",d.text)

							#just check whether it's really a div i am interested in
							#if there is no info about bachelor/diploma thesis i just skip it
							if((isBc != None)  != (isMgr != None)):
								for a in d.select('a'):
									#linkToMoreInfo is a link to the actual thesis
									linkToMoreInfo = a['href']
									if linkToMoreInfo[-1:] == '/':
										if linkToMoreInfo[:1] == '/':

											thesisType = "bc"
											if(isMgr):
												thesisType = "mgr"

											#parse the information I need and save it to a structure
											rokObhajoby = re.search("Rok: [0-9][0-9][0-9][0-9]",d.text).group()[-5:]
											nazevPrace = a.find('i').text
											linkWithInfo = LinkToThesis(thesisType,nazevPrace,rokObhajoby,linkToMoreInfo)
											collectedLinks.append(linkWithInfo);

	return collectedLinks


#oneLinkWithInfo structure that contains basic information and link to the thesis page
def parseInfoFromThesisPage(oneLinkWithInfo):
	#create link to the thesis page

	dataToReturn = []	
	dataToReturn.append(oneLinkWithInfo.bcOrMgr)
	dataToReturn.append(oneLinkWithInfo.nameOfThesis)
	dataToReturn.append(oneLinkWithInfo.year)	
	dataToReturn.append("TODO_parse_vedouci")
	dataToReturn.append("TODO_parse_oponent")
	return dataToReturn


	#I have difficulties to parse the individual thesis pages
	#I will try to fix this later, but I have to move on

	'''finallink = "https://is.ambis.cz"+oneLinkWithInfo.link

	raw_inner_html = simple_get(finallink)
	inner_html = BeautifulSoup(raw_inner_html, 'html.parser')

	dataToReturn = []
	dataToReturn.append(oneLinkWithInfo.bcOrMgr)
	dataToReturn.append(oneLinkWithInfo.nameOfThesis)
	dataToReturn.append(oneLinkWithInfo.year)

	#go down the dom hierarchy
	for div in inner_html.select('div'):
		if 'class' in div.attrs:
			#div that we are interested in
			if div['class'][0] == 'oddil':
				texts = []
				for h3 in div.select('h3'):
					texts.append(h3.text)
				for h4 in div.select('h4'):
					texts.append(h4.text)
				for ul in div.select('h4'):
					texts.append(h4.text)
	return dataToReturn;'''

if(sys.argv[1] == "onlyBasic" or sys.argv[1] == "both"):
	with open('basicData.csv', 'w', newline='', encoding='utf-8') as csvfile:
		advancedDatatWriter = csv.writer(csvfile, delimiter='%',
	                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
		advancedDatatWriter.writerow(['type'] + ["name"] + ["year"] + ["link"])	

		until = 2501
		if len(sys.argv) > 2:
			until = int(sys.argv[2])

		rok = '-'
		if len(sys.argv) > 3:
			rok = sys.argv[3]

		
		for i in range(1,until,50):
			raw_html = simple_get("https://is.ambis.cz/thesis/?FAK=6110;PRI=-;ROK="+rok+";TIT=-;PRA=-;vypsat=1;exppar=1;por="+str(i))
			print(str(i)+". html length: ")
			print(len(raw_html))
			html = BeautifulSoup(raw_html, 'html.parser')
			collectedInfo = parseBasicInfo(html)
			for rowBasicInfo in collectedInfo:
				row = []
				row.append(str(rowBasicInfo.bcOrMgr))
				row.append(str(rowBasicInfo.nameOfThesis))
				row.append(str(rowBasicInfo.year))
				row.append(str(rowBasicInfo.link))
				#row = ['a'] + ['b'] + ['c'] + ['d']
				print(row)
				advancedDatatWriter.writerow( row )	

if sys.argv[1] == "onlyAdvanced":
	with open('basicData.csv', newline='', encoding='utf-8') as csvfile:
		basicDataReader = csv.reader(csvfile, delimiter='%', quotechar='|')
		collectedInfo = []
		skip = 1
		for row in basicDataReader:											
			if(skip):
				skip = 0
				continue
			linkWithInfo = LinkToThesis(row[0],row[1],row[2],row[3])
			collectedInfo.append(linkWithInfo)

if(sys.argv[1] == "onlyAdvanced" or sys.argv[1] == "both"):
	with open('advancedData.csv', 'w', newline='', encoding='utf-8') as csvfile:
		advancedDatatWriter = csv.writer(csvfile, delimiter='%', quotechar='|', quoting=csv.QUOTE_MINIMAL)
		advancedDatatWriter.writerow(['type'] + ["name"] + ["year"] + ["vedouci"] + ["oponent"])	
		
		for oneLinkWithInfo in collectedInfo:
			finalDataToWrite = parseInfoFromThesisPage(oneLinkWithInfo)
			for i in range(0,len(finalDataToWrite)):
				#print(finalDataToWrite[i])
				row.append(finalDataToWrite[i])
			advancedDatatWriter.writerow(row)
			
