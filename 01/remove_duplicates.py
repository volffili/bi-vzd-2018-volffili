import csv

withoutDuplicates = []

with open('advancedData.csv', 'r', newline='', encoding='utf-8') as csvfile:
	advancedDataReader = csv.reader(csvfile, delimiter='%', quotechar='|', quoting=csv.QUOTE_MINIMAL)

	deleted = False
	prevName = ""

	for row in advancedDataReader:
		if row[1] == prevName:
			if deleted == False:
				withoutDuplicates = withoutDuplicates[:-1]
				deleted = True
				continue;
		else:
			deleted = False
			withoutDuplicates.append(row)
		prevName = row[1]


with open('finalData.csv', 'w', newline='', encoding='utf-8') as csvfile:
	advancedDataWriter = csv.writer(csvfile, delimiter='%', quotechar='|', quoting=csv.QUOTE_MINIMAL)

	for rowToWrite in withoutDuplicates:
		advancedDataWriter.writerow(rowToWrite)